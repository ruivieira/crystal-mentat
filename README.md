# mentat

A Crystal library for scientific computing with a focus on data science and machine learning.

# Features

* Balanced Box-Decomposition Trees
* Monotonic Cubic Spline Interpolation