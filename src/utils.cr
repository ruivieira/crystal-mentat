module Mentat extend self

    module Utils extend self

    # Calculates the squared distance between vector *x* and *y*.
    def squared_distance(x : Array(Float64), y : Array(Float64)) : Float64
        sum = 0.0
        (0...x.size).each { |i| sum += (x[i] - y[i])**2 }
        return sum
    end

    end

end