module Mentat extend self

    class SplineInterpolator
        @X = Array(Float64).new
        @Y = Array(Float64).new
    
        def initialize(x : Array(Float64), y : Array(Float64))
        
            if x.size != y.size || x.size < 2
                raise "inputs must have the same size."
            end
    
            @X = x
            @Y = y
    
            n = @X.size
    
            d = Array(Float64).new(n - 1, 0.0)
    
            # calculate Ms
            @M = Array(Float64).new(n, 0.0);
    
            (0...(n-1)).each { |i|
                h = @X[i+1] - @X[i]
                if h <= 0.0
                    raise "X must be monotonically increasing"
                end
                d[i] = (@Y[i+1] - @Y[i]) / h
            }
    
            @M[0] = d[0]
            (1...(n-1)).each { |i|
                @M[i] = (d[i - 1] + d[i]) * 0.5
            }
            @M[n - 1] = d[n - 2]
    
            (0...(n-1)).each { |i|
                if d[i] == 0.0
                    @M[i] = 0.0
                    @M[i + 1] = 0.0
                else
                    a = @M[i] / d[i]
                    b = @M[i + 1] / d[i]
                    h = Math.hypot(a, b)
                    if h > 3.0
                        t = 3.0 / h
                        @M[i] = t * a * d[i]
                        @M[i + 1] = t * b * d[i]
                    end
                end
            }
        end
    
        def hermite(index : Int32, h : Float64, t : Float64) : Float64
            return (@Y[index] * (1 + 2 * t) + h * @M[index] * t) * (1 - t) * (1 - t) + (@Y[index + 1] * (3 - 2 * t) + h * @M[index + 1] * (t - 1)) * t * t
        end
    
        def interpolate(x : Float64) : Float64
            n = @X.size
            
            if x <= @X[0]
                return @Y[0]
            end
    
            if x >= @X[n - 1]
                return @Y[n - 1]
            end
    
            i = 0
            while x >= @X[i + 1]
                i += 1
                if x == @X[i]
                    return @Y[i]
                end
            end
    
            h = @X[i + 1] - @X[i]
            t = (x - @X[i]) / h
    
            return hermite(i, h, t)
        end
    
    end
    
    end