require "logger"

LOG = Logger.new(STDOUT)
LOG.level = Logger::DEBUG


module Mentat extend self

    class TreeNode
    end

    class BBDTreeNode < TreeNode

        property count = 0
        property indices = 0
        getter center
        getter radius
        property cost = 0.0
        property sum
        property lower, upper
        property leaf

        def initialize(@dimension : Int32)
            @lower = uninitialized BBDTreeNode
            @upper = uninitialized BBDTreeNode
            @center = Array(Float64).new(@dimension, 0.0)
            @radius = Array(Float64).new(@dimension, 0.0)
            @sum = Array(Float64).new(@dimension, 0.0)
            @leaf = false
        end

        # Calculate the box boundaries for this node, giving the lower and upper bound vectors
        # returns the `max_radius` and `split`.
        def box(lower : Array(Float64), upper : Array(Float64)) : Tuple(Float64, Int32)
            # calculate boundaries of the node
            max_radius = -1.0
            split = -1
            puts "@dimension = #{@dimension}"
            (0...@dimension).each { |i|
                @center[i] = (lower[i] + upper[i]) / 2.0
                @radius[i] = (upper[i] - lower[i]) / 2.0
                if @radius[i] > max_radius
                    max_radius = @radius[i]
                    split = i
                end
            }
            return {max_radius, split}
        end

        # calculate the node's cost given a centre
        def calculate_cost(mean : Array(Float64)) : Float64
            scatter = 0.0
            (0...@dimension).each { |i|
                x = (@sum[i] / @count.to_f64) - mean[i]
                scatter += x * x
            }
            return @cost + @count.to_f64 * scatter
        end
    end

    class BBDTree

        getter indices : Array(Int32)
        @root : BBDTreeNode
        getter membership
        getter sums
        getter counts
        @dimension : Int32

        def initialize(@data : Array(Array(Float64)))
            n = @data.size

            if @data.empty?
                raise "Dataset cannot be empty."
            end

            @dimension = @data[0].size
            @indices = (0...n).to_a
            @membership = Array(Int32).new
            @sums = Array(Array(Float64)).new
            @counts = Array(Int32).new

            @root = make(0, n)
        end

        def make(start : Int32, finish : Int32) : BBDTreeNode
        
            node = BBDTreeNode.new @dimension

            node.count = finish - start
            node.indices = start

            # calculate the bounding box
            lower_bound = Array(Float64).new(@dimension, 0.0)
            upper_bound = Array(Float64).new(@dimension, 0.0)

            (0...@dimension).each { |i| lower_bound[i] = upper_bound[i] = @data[@indices[start]][i] }

            ((start+1)...finish).each { |i|
                (0...@dimension).each { |j|
                    c = @data[@indices[i]][j]
                    if lower_bound[j] > c
                        lower_bound[j] = c
                    end
                    if upper_bound[j] < c
                        upper_bound[j] = c
                    end
                }
            }

            # calculate bounding box
            max_radius, split = node.box(lower_bound, upper_bound)
            
            # whether this should be a leaf node or not
            if (max_radius < 1e-10)
                        
                node.sum = (0...@dimension).map { |i| @data[@indices[start]][i] }

                if finish > start + 1
                    len = finish - start
                    (0...@dimension).each { |i| node.sum[i] *= len }
                end

                node.cost = 0.0
                node.leaf = true
                return node
            end

            split_cutoff = node.center[split]
            index_a = start
            index_b = finish - 1
            size = 0
            while index_a <= index_b
                accept_index_a = (@data[@indices[index_a]][split] < split_cutoff)
                accept_index_b = (@data[@indices[index_b]][split] >= split_cutoff)

                if !accept_index_a && !accept_index_b
                    @indices.swap(index_a, index_b)
                    accept_index_a = accept_index_b = true
                end

                if accept_index_a
                    index_a += 1
                    size += 1
                end

                if accept_index_b
                    index_b -= 1
                end
            end

            node.lower = make(start, start + size)
            node.upper = make(start + size, finish)

            node.sum = (0...@dimension).map { |i| node.lower.sum[i] + node.upper.sum[i] }
            mean = (0...@dimension).map { |i| node.sum[i] / node.count.to_f64 }

            node.cost = node.lower.calculate_cost(mean) + node.upper.calculate_cost(mean)
            return node
        end


        def clustering(centroids : Array(Array(Float64))) : Float64
            k = centroids.size

            @membership = Array(Int32).new(@indices.size, 0)
            @sums = Array(Array(Float64)).new(k, Array(Float64).new(@dimension, 0.0))
            @counts = Array(Int32).new(k, 0)
            candidates = (0...k).to_a;
            return filter(@root, centroids, candidates, k)
        end

        # Update nodes and clusters
        def filter(node : BBDTreeNode, centroids : Array(Array(Float64)), candidates : Array(Int32), k : Int32) : Float64
        
            min_dist = Mentat::Utils.squared_distance(node.center, centroids[candidates[0]])
            closest = candidates[0]
            (1...k).each { |i|
                dist = Mentat::Utils.squared_distance(node.center, centroids[candidates[i]])
                if dist < min_dist
                    mind_dist = dist
                    closest = candidates[i]
                end
            }

            if !node.leaf
                new_candidates = Array(Int32).new(k, 0)
                new_k = 0

                (0...k).each { |i|
                    if !closer(node.center, node.radius, centroids, closest, candidates[i])
                        new_candidates[new_k] = candidates[i]
                        new_k += 1
                    end
                }

                if new_k > 1
                    result = filter(node.lower, centroids, new_candidates, new_k) + filter(node.upper, centroids, new_candidates, new_k)
                    return result
                end
            end

            (0...@dimension).each { |i| @sums[closest][i] += node.sum[i] }

            @counts[closest] += node.count

            last = node.indices + node.count
            (node.indices...last).each { |i| @membership[indices[i]] = closest }

            return node.calculate_cost(centroids[closest])
        end

        def closer(center : Array(Float64), radius : Array(Float64), centroids : Array(Array(Float64)), best : Int32, test : Int32)
        
            if best == test
                return false
            end

            best = centroids[best]
            test = centroids[test]
            left = 0.0
            right = 0.0
            (0...@dimension).each { |i|
                diff = test[i] - best[i]
                left += diff * diff
                if diff > 0
                    right += (center[i] + radius[i] - best[i]) * diff
                else
                    right += (center[i] - radius[i] - best[i]) * diff
                end
            }
            return (left >= 2 * right)
        end

    end

end