require "./spec_helper"
require "matplotcr"

describe Mentat do
  describe "Monotonic Cubic Splines" do
    it "must produce a MCSI" do
      x = [0.0, 1.0, 4.0, 8.0]
      y = [1.1, 3.3, 7.9, 8.3]
      interpolator = Mentat::SplineInterpolator.new(x, y)
      x2 = Array(Float64).new
      y2 = Array(Float64).new
      (0...100).each { |p|
        pp = p.to_f / 10.0
        x2.push pp
        y2.push interpolator.interpolate(pp)
      }

      puts y2

      figure = Matplotcr::Figure.new

      plot1 = Matplotcr::ScatterPlot.new x, y, colour="orange"
      plot2 = Matplotcr::LinePlot.new x2, y2
      figure.add plot1
      figure.add plot2
      # figure.save "docs/images/mcsi.png"

    end
end
end