require "./spec_helper"
require "matplotcr"

describe Mentat do
  describe "BBD tree" do
    it "must have correct defaults" do
      tree = Mentat::BBDTree.new [[1.1, 2.2], [3.3, 4.4], [5.5, 6.6]]
      tree.indices.size.should eq 3
    end
    it "must have upper and lower uninitialized" do
      node = Mentat::BBDTreeNode.new 3
      puts node.lower
    end
    it "must find neighbours" do
      tree = Mentat::BBDTree.new [[1.1, 1.1], [30.3, 40.4], [30.5, 30.6]]
      r = tree.clustering([[1.1, 1.1], [2.0, 2.0]])
      puts tree.membership
      tree.membership.should eq [0, 1, 1]
    end
end
end